package botiga_de_vins;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Junit_vins {
	
//primera prova, Client Pere, sense descompte
	@Test
	void test_preus_vins_0() {
		double res = vins_i_covid.preus(1,0);
		assertEquals(15,  res);
		//fail("Not yet implemented");
	}
	
	@Test
	void test_preus_vins_1() {
		double res = vins_i_covid.preus(2,15);
		assertEquals(30,  res);
		//fail("Not yet implemented");
	}
	@Test
	void test_preus_vins_2() {
		double res = vins_i_covid.preus(3,30);
		assertEquals(45,  res);
		//fail("Not yet implemented");
	}
	@Test
	void test_preus_vins_3() {
		double res = vins_i_covid.preus(4,45);
		assertEquals(60,  res);
		//fail("Not yet implemented");
	}
	//cas d'error
	@Test
	void test_preus_vins_error_0() {
		double res = vins_i_covid.preus(-1,45);
		assertEquals(45,  res);
		//fail("Not yet implemented");
	}
	//segona prova, Client Joan, sense descompte 
	@Test
	void test_preus_vins_5() {
		double res = vins_i_covid.preus(300,0);
		assertEquals(24,  res);
		//fail("Not yet implemented");
	}
	//cas d'error
	@Test
	void test_preus_vins_error_1() {
		double res = vins_i_covid.preus(301,0);
		assertEquals(0,  res);
		//fail("Not yet implemented");
	}
	//tercera prova, client Jaume aplicant el descompte
	@Test
	void test_preus_vins_10() {
		double res = vins_i_covid.preus(4,45);
		assertEquals(60,  res);
		//fail("Not yet implemented");
	}
	//Preu sense el descompte de Jaume 
	@Test
	void test_preus_vins_11() {
		double res = vins_i_covid.preus(5,60);
		assertEquals(75,  res);
		//fail("Not yet implemented");
	}
	//cas del descompte de Jaume
	@Test
	void test_descompte_vins_0() {
		double res = vins_i_covid.preu_amb_descompte(75);
		assertEquals(69.75,  res);
		//fail("Not yet implemented");
	}
	//Quarta i �ltima prova, Client Isabel, aplicant descompte
	@Test
	void test_preus_vins_12() {
		double res = vins_i_covid.preus(1,0);
		assertEquals(15,  res);
		//fail("Not yet implemented");
	}
	
	@Test
	void test_preus_vins_13() {
		double res = vins_i_covid.preus(278,15);
		assertEquals(39,  res);
		//fail("Not yet implemented");
	}
	@Test
	void test_preus_vins_14() {
		double res = vins_i_covid.preus(65,39);
		assertEquals(54,  res);
		//fail("Not yet implemented");
	}
	@Test
	void test_preus_vins_15() {
		double res = vins_i_covid.preus(33,54);
		assertEquals(69,  res);
		//fail("Not yet implemented");
	}
	
	@Test
	void test_preus_vins_16() {
		double res = vins_i_covid.preus(45,69);
		assertEquals(84,  res);
		//fail("Not yet implemented");
	}
	@Test
	void test_preus_vins_17() {
		double res = vins_i_covid.preus(110,84);
		assertEquals(92,  res);
		//fail("Not yet implemented");
	}
	//cas d'error entrant un num al preu no integer
	@Test
	void test_preus_vins_error_2() {
		double res = vins_i_covid.preus(110,8.6);
		assertEquals(0,  res);
		//fail("Not yet implemented");
	}
	//cas del descompte de Isabel
		@Test
		void test_descompte_vins_1() {
			double res = vins_i_covid.preu_amb_descompte(92);
			assertEquals(85.56,  res);
			//fail("Not yet implemented");
		}
}
