package botiga_de_vins;



import java.util.ArrayList;

import java.util.Scanner;
/**
 * <h2>BOTIGA DE VINS</h2>
 * @version 2.0
 * @author Pere Matarin Cruz
 * @since 14-03-2021
 */


public class vins_i_covid {
	/**
	 * Main: Distribuidor de tasques
	 */
	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);
		// demanem el nom del client i les ampolles que vol
		int num_ampolles = 0;
		int id_vi = 0;
		String nom = "";
		// definim els casos
		int casos = 0;
		casos = reader.nextInt();
		// recorrem el programa tantes vegades com casos tinguem
		for (int i = 0; i < casos; i++) {
			// declarem l'arrayList de la llista que el client far�
			ArrayList<Integer> llista_compra = new ArrayList<Integer>();
			// declarem el preu final
			double preu_final = 0;
			nom = reader.next();
			num_ampolles = reader.nextInt();
			// demanem les id de les ampolles que vol el client i les afegim a la llista
			for (int x = 0; x < num_ampolles; x++) {
				// introduim les id
				id_vi = reader.nextInt();
				llista_compra.add(id_vi);
				// obtenim el preu sense descompte
				preu_final = preus(id_vi, preu_final);
			}
			// apliquem el descompte si escau
			if (num_ampolles >= 5) {
				preu_final = preu_amb_descompte(preu_final);
			}

			// imprimim el nom del client, les ampolles que vol i el preu final en euros

			System.out.println(nom);

			System.out.println(llista_compra);

			System.out.println(preu_final + "�");

		}
	}// final del Main

	/**
	 * Metode que determina els preus dels productes
	 *@param id_vi El par�metre id_vi defineix quin preu tindr� cada vi segons la seva id
	 *@return El preu final que es va acumulant segons la quantitat de vins que indiqui el client
	 */
	public static double preus(int id_vi, double preu_final) {

		// cas pened�s
		if (id_vi > 0 && id_vi <= 100) {
			preu_final = preu_final + 15;
		}
		// cas Terra Alta
		else if (id_vi > 100 && id_vi <= 150) {
			preu_final = preu_final + 8;
		}
		// cas Rioja
		else if (id_vi > 150 && id_vi <= 250) {
			preu_final = preu_final + 17;
		}
		// cas Rueda
		else if (id_vi > 250 && id_vi <= 300) {
			preu_final = preu_final + 24;
		}
		return preu_final;
	}//tancament del m�tode 

	/**
	 *@param preu_final El parametre preu_final entra segons la quantitat que han indicat(5 o mes)
	 *@return El preu final amb el descompte aplicat
	 */
	public static Double preu_amb_descompte(double preu_final) {

		double descompte = 0;
		descompte = ((preu_final * 7) / 100);
		preu_final = preu_final - descompte;
		return preu_final;

	}//tancament del m�tode 
}//tancament de la clase
